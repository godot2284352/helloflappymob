using Godot;
using System;
using System.Linq;

public partial class main : Node2D
{
	[Export]
	public int GameStartWaitSeconds { get; set; } = 3;

	[Export]
	public double AddPipeMinWaitSeconds { get; set; } = 0.7;

	[Export]
	public double AddPipeMaxWaitSeconds { get; set; } = 2;

	[Export]
	public float KontrollImpulsStaerke { get; set; } = -5;

	[Export]
	public float GapSizeModifier { get; set; } = 3;

	[Export]
	public int PipeSpeed { get; set; } = 250;

	private player player;
	private Label timerLabel;
	private Label punkteLabel;

	private GameStates gameState = GameStates.End;
	private int punkte = 0;
	private Timer startTimer = new Timer();

	private Timer addPipeTimer = new Timer();

	enum GameStates
	{
		Starting,
		Running,
		End
	}

	public override void _Ready()
	{
		startTimer.OneShot = true;
		startTimer.Autostart = false;
		startTimer.WaitTime = GameStartWaitSeconds;
		startTimer.Timeout += StartTimer_Timeout;

		addPipeTimer.OneShot = true;
		addPipeTimer.Autostart = false;
		addPipeTimer.WaitTime = GD.RandRange(AddPipeMinWaitSeconds, AddPipeMaxWaitSeconds);
		addPipeTimer.Timeout += AddPipeTimer_Timeout;

		AddChild(startTimer);
		AddChild(addPipeTimer);

		timerLabel = GetNode<Label>("TimerLabel");
		punkteLabel = GetNode<Label>("PunkteLabel");
	}

	public override void _PhysicsProcess(double delta)
	{
		switch (gameState)
		{
			case GameStates.Starting:
				break;

			case GameStates.Running:
				if (Input.IsActionPressed("ui_accept") || Input.IsMouseButtonPressed(MouseButton.Left))
				{
					player.ApplyCentralImpulse(new Vector2(0, KontrollImpulsStaerke));
				}
				processGame();
				break;

			case GameStates.End:
				if (Input.IsActionPressed("ui_accept"))
				{
					gameState = GameStates.Starting;

					startGame();
				}
				break;
		}

		void processGame()
		{
			if (player != null)
			{
				var xToPass = player.Position.X
								- player.GetNode<CollisionShape2D>("CollisionShape2D").Shape.GetRect().Size.X;
				var pipesToPass = GetChildren().OfType<Pipe>().Where(p => p.Passed == false);

				var pipesPassed = pipesToPass.Where(p => p.Position.X < xToPass);
				if (pipesPassed.Any())
				{
					foreach (var p in pipesPassed.ToList())
					{
						punkte++;
						p.Passed = true;
					}
					UpdatePunkteLabel();
				}

				GD.Print($"xToPass: {xToPass}; #pipes: {pipesToPass.Count()}");
			}
		}
	}

	private void startGame()
	{
		createPlayer();

		startTimer.Start();
		punkte = 0;
		UpdatePunkteLabel();
		timerLabel.Visible = true;
		timerLabel.Text = ((int)startTimer.TimeLeft).ToString();
	}

	private void StartTimer_Timeout()
	{
		player.SetDeferred(nameof(player.Freeze).ToLower(), false);
		timerLabel.Visible = false;
		gameState = GameStates.Running;
	
		createPipes();
		addPipeTimer.Start();
	}
	private void AddPipeTimer_Timeout()
	{
		createPipes();
		
		addPipeTimer.WaitTime = GD.RandRange(AddPipeMinWaitSeconds, AddPipeMaxWaitSeconds);
		addPipeTimer.Start();
	}

	private void UpdatePunkteLabel()
	{
		punkteLabel.Text = punkte.ToString();
	}


	public override void _Process(double delta)
	{
		if (startTimer.TimeLeft > 0)
		{
			timerLabel.Text = ((int)startTimer.TimeLeft).ToString();
		}
	}

	private void createPipes()
	{
		var sizeBetweenPipes = player.GetNode<CollisionShape2D>("CollisionShape2D").Shape.GetRect().Size.Y * GapSizeModifier;
		var screenWidth = GetViewport().GetVisibleRect().Size.X;
		var screenHeight = GetViewport().GetVisibleRect().Size.Y;
		var skyHeight = GetNode<Area2D>("Sky").GetNode<CollisionShape2D>("CollisionShape2D").Shape.GetRect().Size.Y;
		var groundHeight = GetNode<Area2D>("Ground").GetNode<CollisionShape2D>("CollisionShape2D").Shape.GetRect().Size.Y;
		var startX = screenWidth;

		var playableHeight = screenHeight - skyHeight - groundHeight;

		// TODO: Pipe-Hoehe stimmt noch nicht so ganz. Keine Lust mehr zu rechnen..
		var topPipeHeight = 0
						+ (float)GD.RandRange(skyHeight, playableHeight);

		var bottomStartPos = topPipeHeight + sizeBetweenPipes;
		var bottomHeight = screenHeight - topPipeHeight - sizeBetweenPipes;

		var topPipe = addNewPipe(new Vector2(startX, 0), topPipeHeight);
		var bottomPipe = addNewPipe(new Vector2(startX, bottomStartPos), bottomHeight);

		bottomPipe.Passed = true; // We just want to check one Pipe for Points

		//GD.Print($"{screenHeight} {topPipeHeight} {bottomStartPos} {bottomHeight}");
	}

	private Pipe addNewPipe(Vector2 position, float height)
	{
		var pipe = ResourceLoader.Load<PackedScene>("res://pipe.tscn").Instantiate<Pipe>();
		pipe.Position = position;
		pipe.Speed = PipeSpeed;

		var shape = pipe.GetNode<CollisionShape2D>("CollisionShape2D");
		var colorRect = pipe.GetNode<ColorRect>("ColorRect");

		pipe.Transform = new Transform2D(0.0f, new Vector2(1, height / colorRect.Size.Y), 0.0f, position);
		/*
		colorRect.SetSize(new Vector2(colorRect.Size.X, height));
		
		shape.Position = new Vector2(colorRect.Size.X, height);
		*/

		pipe.BodyEntered += Pipe_BodyEntered;
		//pipe.AreaShapeEntered += Pipe_AreaShapeEntered;
		//pipe.AreaEntered += Pipe_AreaEntered;
		shape.ChildEnteredTree += Shape_ChildEnteredTree;

		AddChild(pipe);

		return pipe;
	}

	private void Pipe_BodyEntered(Node2D body)
	{
		GD.Print($"Pipe_BodyEntered: {body.Name} {body}");
	}

	private void Pipe_AreaShapeEntered(Rid areaRid, Area2D area, long areaShapeIndex, long localShapeIndex)
	{
		GD.Print($"Pipe_AreaShapeEntered: {area.Name} {area}");
	}

	private void Pipe_AreaEntered(Area2D area)
	{
		GD.Print($"Pipe_AreaEntered: {area.Name} {area}");
	}

	private void Shape_ChildEnteredTree(Node node)
	{
		GD.Print($"Shape_ChildEnteredTree: {node.Name} {node}");
	}
	private void OnGroundBodyEntered(Node2D body)
	{
		GD.Print($"OnGroundBodyEntered: {body.Name} {body}");

		return;
		player.SetDeferred(nameof(player.Freeze).ToLower(), true);

		gameState = GameStates.End;
		timerLabel.SetDeferred(nameof(timerLabel.Text).ToLower(), "Press Space to Start");
		timerLabel.SetDeferred(nameof(timerLabel.Visible).ToLower(), true);
	}


	private void createPlayer()
	{
		if (player != null)
		{
			RemoveChild(player);
			player.QueueFree();
		}

		player = ResourceLoader.Load<PackedScene>("res://player.tscn").Instantiate<player>();
		//player.Mass = 0;
		player.Freeze = true;

		AddChild(player);
	}
}
