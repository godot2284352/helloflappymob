using Godot;
using System;

public partial class Pipe : Area2D
{
	public int Speed { get; set; } = 250;

	public bool Passed { get; set; } = false;

	public override void _Ready()
	{
		AreaExited += (b) => { QueueFree(); };
		ZIndex = -1;
	}

	public override void _Process(double delta)
	{
		var velocity = new Vector2(Speed, 0);

		Position -= velocity * (float)delta;
	}

}
